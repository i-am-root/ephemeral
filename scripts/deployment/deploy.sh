#!/usr/bin/env bash

# Go to the Drupal root.
cd "./web"
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# Clear drush's cache.
drush cache-clear drush
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# Update database operations.
drush updb -y
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# Update entities structure from the code.
drush entup -y
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# Import the exported configuration.
drush cim -y
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# Update local
drush locale-check
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
drush locale-update
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# Clear Drupal's cache.
drush cr
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
